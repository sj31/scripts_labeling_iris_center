function theCost = ellipseCost(A)

global FF Yg

out = manduEllipse(A,size(FF));
    
theCost = -sum(FF(out)); 

%figure(1), clf, colormap(gray)
%YYg = Yg; YYg(out) = 255;  imagesc(YYg),drawnow,