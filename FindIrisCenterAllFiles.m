function FindIrisCenterAllFiles(dirname)

a = dir(dirname);

disp(a);
for i=3:length(a)
    fname = a(i).name;
    [~,~,ext] = fileparts(fname);
    if strcmp(ext,'.jpg')
        disp(fname)
        fnamecsv = [fname(1:strfind(fname,'jpg')-1),'csv'];
        disp(exist(fnamecsv, 'file'))
        fid = fopen(fullfile(a(i).folder,fnamecsv),'w');
        [c_left,c_right] = manduFindIrisCenter(fullfile(a(i).folder,fname));
        fprintf(fid,'Left iris center,%f,%f\n Right iris center,%f,%f',c_left(1),c_left(2),c_right(1),c_right(2));
        fclose(fid)
        end
    end
end
    