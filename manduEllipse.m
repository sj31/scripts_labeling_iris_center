function out = manduEllipse(A,theSize);

a1 = A(1);a2=A(2);x0=A(3);y0=A(4);

yy = zeros(theSize);

y1 = repmat((1:theSize(2)),[theSize(1),1]);
y2 = repmat((1:theSize(1))',[1,theSize(2)]);

yy = (y1-x0).^2/a1 + (y2-y0).^2/a2 - 1;

u = bwperim(yy>0);
u(:,1) = 0; u(1,:) = 0; u(:,end) = 0; u(end,:) = 0;
out = find(u);


% The following is totally useless!
% for i = 1:length(out)
%     [y,x] = ind2sub(theSize,out(i)); 
%     Ax(i,1) = (x-x0)/a1/2;
%     Ax(i,2) = 0;
% %    Ax(i,2) = (y-y0)^2/a2 * a1/(x-x0)/2;
%     Ax(i,3) = 1;
%     Ax(i,4) = 0;
% %    Ax(i,4) = (y-y0)/a2 * a1/(x-x0);
%     Ay(i,2) = (y-y0)/a2/2;
% %    Ay(i,2) = (x-x0)^2/a1 * a2/(y-y0)/2;
%     Ay(i,1) = 0;
% %    Ay(i,3) = (x-x0)/a1 * a2/(y-y0);
%     Ay(i,3) = 0;
%     Ay(i,4) = 1;
% end
    