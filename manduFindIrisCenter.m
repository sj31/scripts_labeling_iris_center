function [c_left,c_right,file] = manduFindIrisCenter(file)
global Yg FF
if nargin==0
    [file,path] = uigetfile('*.jpg');
    Y = imread(fullfile(path,file));
else
    Y = imread(file);
end
figure(1), colormap(gray)
Yg = rgb2gray(Y); Yg = double(Yg);
[Fx,Fy] = gradient(imgaussfilt(Yg,3)); FFor = abs(Fx);
imagesc(Yg), axis image
YYg = Yg; YYg0 = YYg;
for i = 1:2
    HAPPY = 0;
     while ~HAPPY
       figure(1)
        if i==1
            title('Click on left eye','FontSize', 24)
        else
            title('Click on right eye','FontSize', 24)
        end
        [x,y] = ginput(1);
        YYg(round(y),round(x)) = 150;

        uu = zeros(size(Yg));
        uu(round(y),round(x)) = 1;
        uu = filter2(fspecial('gaussian',300,20),uu);
        FF = FFor .* uu;

        A = [10^2,10^2,x,y]; % initial values that seem to work

        A = fminsearch(@ellipseCost,A);
        out = manduEllipse(A,size(FF));
        YYg(out) = 255; YYg(round(A(4)),round(A(3))) = 255; imagesc(Yg),axis image
        YYg0(round(A(4)),round(A(3))) = 255;
        %disp(['x = ',int2str(round(A(3))),', y = ',int2str(round(A(4)))])

        cx = round(A(3)); cy  = round(A(4));

        figure(i+1),clf, colormap(gray), imagesc(YYg(cy-50:cy+50,cx-75:cx+75)), axis image
        DONE = 0; SHOW_ELLIPSE = 1;
        while ~DONE
            title('[T]oggle/[S]ave/[R]edo');
            k = waitforbuttonpress;
            value = double(get(gcf,'CurrentCharacter'));
            if (value==116) || (value==84) % t or T
                if SHOW_ELLIPSE
                    SHOW_ELLIPSE = 0;
                    figure(i+1),clf, colormap(gray), imagesc(YYg0(cy-50:cy+50,cx-75:cx+75)), axis image
                else
                    SHOW_ELLIPSE = 1;
                    figure(i+1),clf, colormap(gray), imagesc(YYg(cy-50:cy+50,cx-75:cx+75)), axis image
                end
            elseif (value==115) || (value==118) % s or S
                figure(i+1),clf, colormap(gray), imagesc(YYg(cy-50:cy+50,cx-75:cx+75)), axis image
                DONE = 1;
                if i==1
                    c_left = A(3:4);
                else
                    c_right = A(3:4);
                end
                HAPPY = 1;
            elseif (value==114) || (value==82) % r or R
                DONE = 1;
                YYg = Yg; YYg0 = Yg;
            end
        end
    end
end