# MATLAB code to label iris centers


    
### Scripts to label iris center instructions

   Using MATLAB code, run the foollowing command.
   
   -----------------------

    
   **FindIrisCenterAllFiles(<path to image folder\>)**
    
    
   -----------------------

    
   It will save label csv files in the same folder as images.
